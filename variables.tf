variable "aws_region" {
  default = "eu-central-1"
}

variable "vpc_name" {
  default = "endurosat-vpc"
}

variable "db_engine_version" {
  default = "15.4"
}

variable "db_instance_class" {
  default = "db.t3.small"
}

variable "db_storage" {
  default = "20"
}

variable "db_name" {
  default = "endurosat_pg"
}

variable "lambda_function_name" {
  default = "endurosat_telemetry_analyzer"
}

variable "apprunner_service_name" {
  default = "endurosat_apprunner"
}