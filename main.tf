module "vpc" {
  source  = "./modules/vpc"
  name = var.vpc_name
}

module "rds" {
  source = "./modules/rds"
  vpc_id = module.vpc.vpc_id
  cidr_blocks = module.vpc.vpc_cidr_block
  database_subnet_group = module.vpc.database_subnet_group
  engine_version = var.db_engine_version
  instance_class = var.db_instance_class
  allocated_storage = var.db_storage
  identifier = var.db_name
}

module "lambda" {
  source = "./modules/lambda"
  function_name = var.lambda_function_name
  private_subnet = module.vpc.database_subnet_group
  sg_ids = module.rds.security_group_id
}

module "apprunner" {
  source = "./modules/apprunner"
  service_name = var.apprunner_service_name
  subnets = module.vpc.database_subnet_group
  sg_ids = module.rds.security_group_id
}