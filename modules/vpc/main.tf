data "aws_availability_zones" "available" {}

locals {
  cidr = "10.0.0.0/16"
  azs = slice(data.aws_availability_zones.available.names, 0, 3)
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.5.1"
  name = var.name
  azs = local.azs
  database_subnets = [for k, v in local.azs : cidrsubnet(local.cidr, 8, k)]
  public_subnets  = [for k, v in local.azs : cidrsubnet(local.cidr, 8, k + 48)]
  

  enable_nat_gateway      = true
  single_nat_gateway      = true
  enable_dns_hostnames    = true
  create_database_internet_gateway_route = true
  create_database_subnet_route_table = true
  create_database_subnet_group = true
}