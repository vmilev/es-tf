module "lambda" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "7.2.1"

  function_name = var.function_name
  handler       = "index.lambda_handler"
  runtime       = "python3.8"

  source_path = "../src/lambda-function"

  attach_policies = true
  policies        = ["arn:aws:iam::aws:policy/AWSLambdaVPCAccessExecutionRole", "arn:aws:iam::aws:policy/AmazonRDSFullAccess"]
  number_of_policies = 2

  vpc_subnet_ids                     = var.private_subnet
  vpc_security_group_ids             = [var.sg_ids]
  attach_network_policy              = true
  replace_security_groups_on_destroy = true
  replacement_security_group_ids     = [var.sg_ids]
}

