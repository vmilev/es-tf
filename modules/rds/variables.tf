# modules/rds_postgresql/variables.tf

variable "allocated_storage" {
  description = "Allocated storage for the RDS instance (in GB)"
  type        = number
}

variable "engine_version" {
  description = "PostgreSQL engine version"
}

variable "instance_class" {
  description = "RDS instance class"
}

variable "identifier" {
  description = "Name of the cluster"
}

variable "vpc_id" {
  
}

variable "cidr_blocks" {
  
}

variable "database_subnet_group" {
  
}

