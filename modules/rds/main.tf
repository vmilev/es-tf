module "rds" {
  source  = "terraform-aws-modules/rds/aws"
  version = "6.3.1"
  engine                                = "postgres"
  family                                = "postgres15"
  ca_cert_identifier                    = "rds-ca-rsa2048-g1"
  db_subnet_group_name                  = var.database_subnet_group
  engine_version                        = var.engine_version
  instance_class                        = var.instance_class
  allocated_storage                     = var.allocated_storage
  apply_immediately                     = true
  backup_retention_period               = 7
  deletion_protection                   = true
  performance_insights_enabled          = true
  performance_insights_retention_period = 7
  identifier                            = var.identifier
  manage_master_user_password           = true
  vpc_security_group_ids = [module.security_group.security_group_id]
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 5.0"

  name        = var.identifier
  description = "Complete PostgreSQL example security group"
  vpc_id      = var.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 5432
      to_port     = 5432
      protocol    = "tcp"
      description = "PostgreSQL access from within VPC"
      cidr_blocks = var.cidr_blocks 
    },
  ]
}