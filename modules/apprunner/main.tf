module "app-runner" {
  source  = "terraform-aws-modules/app-runner/aws"
  version = "1.2.0"
  service_name = var.service_name
  create_vpc_connector          = true
  vpc_connector_subnets         = var.subnets
  vpc_connector_security_groups = [var.sg_ids]
  network_configuration = {
    egress_configuration = {
      egress_type = "VPC"
    }
  }
}